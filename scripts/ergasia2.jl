### A Pluto.jl notebook ###
# v0.19.25

using Markdown
using InteractiveUtils

using Plots, StatsBase,LaTeXStrings,FFTW

include("../src/Bispectrum.jl")
include("../src/Parzen.jl")

using .Bispectrum

λ = [
	0.12;
	0.3;
	0.42;
	0.19;
	0.17;
	0.36
]
ω = 2π*λ
ϕ = 2π*rand(6)
ϕ[3] + ϕ[1] + ϕ[2]
ϕ[6] + ϕ[4] + ϕ[5]
N = 8192
k = collect(0:N-1)
x = sum( cos.( ω*k' .+ ϕ ), dims=1 )';

# Power Spectrum
L₂ = 128
lags = collect(-L₂:L₂)
r₂ = autocor(x, 0:L₂-1);
C₂ = fft(r₂);
n = 0:L₂-1
dω = 2π/L₂
ω_span = -π:dω:(π-dω)
f_span = ω_span/2π


# Direct Method
K, M = 32, 256
L₃ = 64

println("Estimating Bispectrum...")
@time fspan, B = bispectrum(x, K,M, 1, method=:direct)
println("Done!")

print(B)

p1 = heatmap(
    fspan,
    fspan,
    abs.(B),
    xlabel=L"f_1 (Hz)",
    ylabel=L"f_2 (Hz)"
)
savefig(p1, "hm.png")

