#
# bispectrum.jl
#
# This file implements function for estimating the bispectrum from data
#
# Author: Alexandros Tsingilis
# Date: 20230509
#

module Bispectrum

using Statistics;
using FFTW;

export bispectrum,mom3


function bispectrum(x, K, M; method=:indirect, kwargs...)
    
    # Get the number of elements in `x`
    N = length(x)
    
    # Split the vector x into subset
    subsets = [ @view x[ (i-1)*M+1 : i*M ] for i=1:K ]

    # Check if maximum lags was given
    if :L in keys(kwargs)
        L = kwargs[:L]
    else
        L = M
    end

    # Check if sampling frequncy was given
    if :fs in keys(kwargs)
        fs = kwargs[:fs]
    else
        fs = 1 # Hz
    end

    # Select window
    if :window in keys(kwargs)
        W = kwargs[:window]
    else
        W = ones(L,L)
    end

    # Take the mean for every `i` in `r[:,:,i]`
    r = zeros(L,L,K)

    # Compute the third order momment
    for (i,xi) in enumerate(subsets)
        # TODO : Use matrix multiplication
        for m = 1:L
            for n = 1:m
                s1 = m
                s2 =  min(M-L, M-m, M-n)
                y1 = @view xi[s1:s2]
                y2 = @view xi[s1+m:s2+m]
                y3 = @view xi[s1+n:s2+n]
                r[m,n,i] = sum( y1 .* y2 .* y3 ) / M;
            end
        end
    end

    # Take the mean
    r = mean(r, dims=3)
    r = reshape(r, (L,L) )

    # Now take the fft
    B̂ = fftshift(fft(W .* r))

    # The spacing bettwen frquencies is
    Δ₀ = fs / L

    # The span of frequencies is
    fspan = -fs/2 : Δ₀ : (fs/2 - Δ₀)

    # Return fspan and estimation
    return fspan[fspan .>= 0], B̂[fspan .>=0, fspan .>= 0]
    
end



function bispectrum(x, K, M, fs; method=:direct)
    Δ₀ = fs/M
    subsets = [ @view x[ (i-1)*M+1 : i*M ] for i=1:K ]
    Y = map( x -> fft(x), subsets )
    B̂ = zeros(Complex, Int(M/2), Int(M/2),K );

    for (i,Yᵢ) in enumerate(Y)
        for n₁ = 1 : Int(M/2)
            for n₂ = 1 : n₁
                B̂[n₁, n₂, i] = Yᵢ[n₁] * Yᵢ[n₂] * conj( Yᵢ[n₁ + n₂] )
            end
        end
    end

    fspan = LinRange(0,fs/2,Int(M/2))

    return fspan, reshape(mean(B̂,dims=3), (Int(M/2),Int(M/2)))
end

end
