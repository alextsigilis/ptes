"""
    get_parzen_win(n)

Create a matrix of lenght `n` containing the values of a parzen window.
"""
function get_parzen_win(n)
    d = zeros(n)
    L = n/2
    # Calculate every value of the 1D parzen window
    for (i,m) in enumerate(LinRange(-L, L, n))
        if abs(m) <= L/2
                d[i] = 1 - 6*( abs(m)/L )^2 + 6*( abs(m)/L)^3
        else
                d[i] = 2*(1 - abs(m)/L)^3
        end
    end
    # Make 2D window
    W = zeros(n,n)
    # Fill the matrxi
    for m = 1:L₃
        for n = 1:m
            W[m,n] = d[m]*d[n]*d[m-n+1]
	end
    end

    return W
end
